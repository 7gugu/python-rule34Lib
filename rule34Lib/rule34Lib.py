# coding:utf-8
import base64
import os
import re
from os import path
from io import StringIO, BytesIO
from urllib import parse
import time
import pycurl


class rule34Lib:
    folderPath = "C:\\rule34"
    tagName = "female"
    startPid = 0
    endPid = 42
    userAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; .NET CLR 3.5.21022; .NET CLR 1.0.3705; .NET CLR 1.1.4322)"
    blackList = []
    proxyMode = False
    proxyAddress = "127.0.0.1"
    proxyPort = "1080"
    proxyUserPwd = ""

    def __init__(self,tag, folderPath, startPid=0, endPid=42):
        self.tagName = tag
        self.folderPath=folderPath
        self.startPid=startPid
        self.endPid=endPid
        print("_____       _      ____  _  _\r\n"
              + "|  __ \     | |    |___ \| || |  \r\n"
              + "| |__) |   _| | ___  __) | || |_ \r\n"
              + "|  _  / | | | |/ _ \|__ <|__   _|\r\n"
              + "| | \ \ |_| | |  __/___) |  | |  \r\n"
              + "|_|  \_\__,_|_|\___|____/   |_|  \r\n"
              + "                                  \r\n")
        print("Start Download:\r\n")

    def setStartPid(self,startPid):
        if startPid >= 0:
            self.startPid=startPid
        else:
            self.startPid=0
            return False
        return True

    def setEndPid(self,endPid):
        if endPid >=1 and endPid >self.startPid :
            self.endPid=endPid
        else:
            self.endPid=self.startPid+1
        return True

    def getStartPid(self):
        return self.startPid

    def getEndPid(self):
        return self.endPid

    def setBlackList(self,listArray=[]):
        self.blackList=listArray
        return True

    def setTagName(self,tagName=False):
        if tagName:
            self.tagName=parse.urlencode(tagName)
            return True
        else:
            return False

    def setProxyServer(self,proxyAddress,proxyPort,proxyUserPwd=""):
        if proxyAddress != "" and proxyPort != "":
            self.proxyMode=True
            self.proxyAddress=proxyAddress
            self.proxyPort=proxyPort
            self.proxyUserPwd=proxyUserPwd
        else:
            self.proxyMode=False
            return False
        return True

    def setFolderPath(self,folderPath=False):
        if folderPath:
            self.folderPath=folderPath
        else:
            return False
        return True

    def timeComp(self):
        now = int(time.time())
        timeStruct = time.localtime(now)
        strTime = time.strftime("%Y-%m-%d %H:%M:%S",timeStruct)
        return "["+strTime+"]"

    def curl(self,url,fileName=""):
        fileStream=""
        output=""
        if url not in self.blackList and url != "":
            curl = pycurl.Curl()
            curl.setopt(pycurl.URL,url)
            curl.setopt(pycurl.HEADER,0)
            curl.setopt(pycurl.CUSTOMREQUEST,'GET')
            curl.setopt(pycurl.TIMEOUT,600)
            curl.setopt(pycurl.FOLLOWLOCATION,1)
            transfer = BytesIO()
            curl.setopt(pycurl.WRITEFUNCTION, transfer.write)
            curl.setopt(pycurl.SSL_VERIFYHOST,False)
            curl.setopt(pycurl.SSL_VERIFYPEER,False)
            curl.setopt(pycurl.SSL_VERIFYPEER,False)
            curl.setopt(pycurl.USERAGENT,self.userAgent)
            if self.proxyMode:
                curl.setopt(pycurl.PROXY,self.proxyAddress)
                curl.setopt(pycurl.PROXYPORT,self.proxyPort)
            if self.proxyUserPwd != "":
                curl.setopt(pycurl.PROXYUSERPWD,self.proxyUserPwd)
            if fileName !="":
                fileName=fileName.strip()
                fullPath=self.folderPath+fileName
                fileStream=open(fullPath,"wb")
                curl.setopt(pycurl.FILE,fileStream)
            curl.perform()
            output=transfer.getvalue()
            if fileName != "":
                fileStream.close()
            if fileName == "":
                self.log("[检索]"+url)
            else:
                self.log("[下载]"+url)
            curl.close()
        else:
            return False
        return output

    def log(self,data):
        data = self.timeComp()+data
        dir = path.dirname(__file__)
        dir = path.dirname(dir)
        if not os.path.exists(dir+"\\log"):
            os.mkdir(dir+"\\log")
        fileStream = open(dir+"\\log\\log.txt",'a')
        output = fileStream.write(data)
        fileStream.close()
        return output

    def imgList(self):
        if self.startPid == 0 :
            string = self.curl('https://rule34.xxx/index.php?page=post&s=list&tags='+self.tagName)
        else:
            string = self.curl('https://rule34.xxx/index.php?page=post&s=list&tags='+self.tagName+'&pid='+str(self.startPid))
        if string != "":
            pattern = re.compile('index.php\?page=post&amp;s=view&amp;id=([^\\s]*)"')
            matches = re.findall(pattern , str(string))
            return matches
        else:
            return False

    def imgDown(self,link=""):
        if link !="" :
            if not os.path.exists(self.folderPath):
                os.mkdir(self.folderPath)
            if not os.path.exists(self.folderPath+"\\download"):
                os.mkdir(self.folderPath+"\\download")
            if not os.path.exists(self.folderPath+"\\download\\jpg"):
                os.mkdir(self.folderPath+"\\download\\jpg")
            if not os.path.exists(self.folderPath+"\\download\\gif"):
                os.mkdir(self.folderPath+"\\download\\gif")
            if not os.path.exists(self.folderPath+"\\download\\webm"):
                os.mkdir(self.folderPath+"\\download\\webm")
            if not os.path.exists(self.folderPath+"\\download\\jpg\\"+self.tagName+"\\"):
                os.mkdir(self.folderPath+"\\download\\jpg\\"+self.tagName+"\\")
            if not os.path.exists(self.folderPath+"\\download\\gif\\"+self.tagName+"\\"):
                os.mkdir(self.folderPath+"\\download\\gif\\"+self.tagName+"\\")
            if not os.path.exists(self.folderPath+"\\download\\webm\\"+self.tagName+"\\"):
                os.mkdir(self.folderPath+"\\download\\webm\\"+self.tagName+"\\")
            string = self.curl(link)
            pattern = re.compile("[a-zA-z]+://[^\\s]*.(?:jpg|webm|jpeg|gif|png)")
            matches = re.findall(pattern,str(string))
            filename = self.tagName+"-"+str(base64.b64encode(link.encode('utf-8')),'utf-8')
            downstate=False
            url = matches[0]
            split = url.split(".")
            if split[3] == "png":
                print("Png Link:" + url + "\r\n")
                downstate = self.curl(url,"\\download\\jpg\\"+self.tagName+"\\"+filename+".png")
            elif split[3] == "jpg":
                print("Jpg Link:" + url + "\r\n")
                downstate = self.curl(url,"\\download\\jpg\\"+self.tagName+"\\"+filename+".jpg")
            elif split[3] == "jpeg":
                print("Jpeg Link:" + url + "\r\n")
                downstate = self.curl(url, "\\download\\jpg\\" + self.tagName + "\\" + filename + ".jpeg")
            elif split[3] == "gif":
                print("Gif Link:" + url + "\r\n")
                downstate = self.curl(url, "\\download\\gif\\" + self.tagName + "\\" + filename + ".gif")
            elif split[3] == "webm":
                print("Webm Link:" + url + "\r\n")
                downstate = self.curl(url, "\\download\\webm\\" + self.tagName + "\\" + filename + ".webm")
            else:
                print("Unkown Link:"+url+"\r\n")
            return downstate
        else:
            return False









